dofile(minetest.get_modpath("steel").."/rust.lua")

if minetest.settings:get_bool("creative_mode") and not minetest.get_modpath("unified_inventory") then
	steel_expect_infinite_stacks = true
else
	steel_expect_infinite_stacks = false
end

function steel_rotate_and_place(itemstack, placer, pointed_thing)

	if minetest.is_protected(pointed_thing.above, placer:get_player_name()) then
		return itemstack
	end
	local above = pointed_thing.above
	local under = pointed_thing.under
	local pitch = placer:get_look_pitch()
	local node = minetest.get_node(above)
	local fdir = minetest.dir_to_facedir(placer:get_look_dir())
	local wield_name = itemstack:get_name()


	local iswall = (above.x ~= under.x) or (above.z ~= under.z)
	local isceiling = (above.x == under.x) and (above.z == under.z) and (pitch > 0)

	if iswall then 
		local dirs = { 2, 3, 0, 1 }
		minetest.add_node(above, {name = wield_name.."_wall", param2 = dirs[fdir+1] }) -- place wall variant
	elseif isceiling then
		minetest.add_node(above, {name = wield_name.."_wall", param2 = 19 }) -- place wall variant on ceiling
	else
		minetest.add_node(above, {name = wield_name }) -- place regular variant
	end

	if not steel_expect_infinite_stacks then
		itemstack:take_item()		
	end
	return itemstack
end

minetest.register_node("steel:plate_soft", {
	description = "Soft steel plate",
	tiles = {"steelplatesoft.png"},
	is_ground_content = true,
	groups = {cracky=2},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("steel:plate_hard", {
	description = "Hardened steel plate",
	tiles = {"steelplatehard.png"},
	is_ground_content = true,
	groups = {cracky=1},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("steel:plate_rusted", {
	description = "Rusted steel plate",
	tiles = {"steel_rusted.png"},
	is_ground_content = true,
	groups = {cracky=1,choppy=1},
	sounds = default.node_sound_stone_defaults(),
})

if minetest.registered_nodes["streets:steel_support"] then
	minetest.register_alias("steel:strut","streets:steel_support")
else
	minetest.register_node("steel:strut", {
		drawtype = "glasslike",
		description = "Strut",
		tiles = {"strut.png"},
		is_ground_content = true,
		paramtype= "light",
		groups = {choppy=1,cracky=1},
		sounds =  default.node_sound_stone_defaults(),
	})
	minetest.register_alias("streets:steel_support","steel:strut")
end
minetest.register_node("steel:grate_soft", {
	description = "Soft Steel Grate",
	drawtype = "fencelike",
	tiles = {"worldgratesoft.png"},
	inventory_image = "gratesoft.png",
	wield_image = "gratesoft.png",
	paramtype = "light",
	is_ground_content = true,
	selection_box = {
		type = "fixed",
		fixed = {-1/7, -1/2, -1/7, 1/7, 1/2, 1/7},
	},
	groups = {cracky=2,choppy=2},
	sounds = default.node_sound_wood_defaults(),
})

minetest.register_node("steel:grate_hard", {
	description = "Hardened Steel Grate",
	drawtype = "fencelike",
	tiles = {"worldgratehard.png"},
	inventory_image = "gratehard.png",
	wield_image = "gratehard.png",
	paramtype = "light",
	is_ground_content = true,
	selection_box = {
		type = "fixed",
		fixed = {-1/7, -1/2, -1/7, 1/7, 1/2, 1/7},
	},
	groups = {cracky=1,choppy=1},
	sounds = default.node_sound_wood_defaults(),
})

minetest.register_craftitem("steel:sheet_metal", {
	description = "Sheet metal",
	inventory_image = "steel_sheet_metal.png",
})

minetest.register_node("steel:roofing", {
	description = "Corrugated steel roofing",
	drawtype = "raillike",
	tiles = {"corrugated_steel.png"},
	inventory_image = "corrugated_steel.png",
	wield_image = "corrugated_steel.png",
	paramtype = "light",
	is_ground_content = true,
	walkable = true,
	selection_box = {
		type = "fixed",
                -- but how to specify the dimensions for curved and sideways rails?
                fixed = {-1/2, -1/2, -1/2, 1/2, -1/2+1/16, 1/2},
	},
	groups = {bendy=2,snappy=1,dig_immediate=2},
	on_place =steel_rotate_and_place,
})

minetest.register_node("steel:roofing_wall", {
	description = "Corrugated steel wall",
	drawtype = "nodebox",
	tiles = {"corrugated_steel.png"},
	inventory_image = "corrugated_steel.png",
	wield_image = "corrugated_steel.png",
	paramtype = "light",
	paramtype2 = "facedir",
	is_ground_content = true,
	walkable = true,
	groups = {bendy=2,snappy=1,dig_immediate=2, not_in_creative_inventory=1},
	drop = "steel:roofing",
	on_place = steel_rotate_and_place,
	node_box = {
			type = "fixed",
			fixed = { -0.5, -0.5, -0.48, 0.5, 0.5, -0.48 }
	},
	selection_box = {
			type = "fixed",
			fixed = { -0.5, -0.5, -0.5, 0.5, 0.5, -0.4 }
	},
})

--steel scrap are only used to recover ingots

minetest.register_craftitem("steel:scrap", {
	description = "Steel scraps",
	inventory_image = "scrap.png",
})

--recipes

minetest.register_craft({
	output = "steel:roofing 3",
	recipe = {
		{ "steel:sheet_metal", "", "steel:sheet_metal" },
		{ "", "steel:sheet_metal", "" },
	}
})

minetest.register_craft({
	output = "steel:sheet_metal 3",
	recipe = {
		{ "", "steel:roofing", "" },
		{ "steel:roofing", "", "steel:roofing" },

	}
})

minetest.register_craft({
	output = "steel:plate_soft 2",
	recipe = {
		{"default:steel_ingot", "default:steel_ingot"},
		{"default:steel_ingot", "default:steel_ingot"},
	}
})

minetest.register_craft({
	type = "cooking",
	output = "steel:plate_hard",
	recipe = "steel:plate_soft",
})

minetest.register_craft({
	output = "steel:grate_soft 3",
	recipe = {
		{"default:steel_ingot", "", "default:steel_ingot"},
		{"default:steel_ingot", "", "default:steel_ingot"},
	}
})

minetest.register_craft({
	type = "cooking",
	output = "steel:grate_hard",
	recipe = "steel:grate_soft",
})

-- only register this craft if streets is not loaded
if not minetest.registered_nodes["streets:steel_support"] then
	minetest.register_craft({
		output = "steel:strut 5",
		recipe = {
			{"default:steel_ingot", "", "default:steel_ingot"},
			{"", "default:steel_ingot", ""},
			{"default:steel_ingot", "", "default:steel_ingot"},
		}
	})
end

minetest.register_craft({
	output = "steel:sheet_metal 6",
	recipe = {
		{"default:steel_ingot", "default:steel_ingot", "default:steel_ingot"},
	}
})

--recycling recipes

minetest.register_craft({
	output = "steel:scrap 2",
	recipe = {
		{"steel:strut"},
	}
})

minetest.register_craft({
	output = "steel:scrap 2",
	recipe = {
		{"steel:grate_soft"},
	}
})

minetest.register_craft({
	output = "steel:scrap 2",
	recipe = {
		{"steel:grate_hard"},
	}
})

minetest.register_craft({
	output = "steel:scrap",
	recipe = {
		{"steel:roofing"},
	}
})

minetest.register_craft({
	output = "steel:scrap",
	recipe = {
		{"steel:sheet_metal"},
	}
})

minetest.register_craft({
	output = "steel:scrap 4",
	recipe = {
		{"steel:plate_soft"},
	}
})

minetest.register_craft({
	output = "steel:scrap 4",
	recipe = {
		{"steel:plate_hard"},
	}
})

if minetest.get_modpath("technic") then
	minetest.register_craft({
		output = "default:iron_lump",
		recipe = {
			{"steel:scrap", "steel:scrap"},
			{"steel:scrap", "steel:scrap"},
		}
	})
else
	minetest.register_craft({
		output = "default:iron_lump",
		recipe = {
			{"steel:scrap", "steel:scrap"},
		}
	})

end

